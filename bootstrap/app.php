<?php

require __DIR__ . '/../vendor/autoload.php';

use Moust\Silex\Provider\CacheServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\TwigServiceProvider;
use App\Providers\ImageServiceProvider;

$app = new Silex\Application([
  'debug' => true
]);

$app->register(new TwigServiceProvider(), array(
  'twig.path' => __DIR__.'/../views',
));

$app->register(new CacheServiceProvider, [
  'cache.options' => [
    'driver' => 'file',
    'cache_dir' => __DIR__ . '/../cache/images'
  ]
]);

$app->register(new DoctrineServiceProvider, [
  'db.options' => [
    'driver' => 'pdo_mysql',
    'host' => 'localhost',
    'dbname' => 'PlacePony',
    'user' => 'CherryStellaris',
    'password' => 'x9putmD3ghuaUmLv',
    'charset' => 'utf8mb4'
  ]
]);

$app->register(new ImageServiceProvider);

require __DIR__ . '/../routes/web.php';
