# PlacePony

A quick and simple service for getting ponies from My Little Pony Friendship is Magic for use as placeholders in your designs or code.
Just put your image size (width & height) after our URL and you'll get a placeholder.
